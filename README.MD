[TOC]

# A STG REST Certification Module

In this project you will be creating a simple REST API, using Node and Express, to solve the Business Problem described below. This project is designed to test your JavaScript, Node, Express and REST Architecture skills.

It is highly recommended to read this entire document before proceeding.

## Before You Begin

To more clearly showcase your REST skill, you will need to follow the following procedural steps.

1. Before you work on each step of this certification you will need create a new branch specifically for that step.
2. You will then need to commit and push your work to your repository on Bitbucket at least daily while working on this certification. While there is no official time limit on completing this certification we want to see the progress that you have made. This does not mean that you need to make a commit daily, it simply means that any day that you do work on the certification you should make a commit and push that work by the end of the day.
3. After each component section is completed you will need to save and push your code AND make a pull/merge request to merge your code to your repository's master branch for each section of your work. (Please do not delete the merged branch from your repository!)
4. The key understanding point to this certification is REST Architecture.  A common question a developer may ask is what methods or operations do I need as a guide in helping me.  For this certification we want to avoid answering that question.  Answering that would take away from a key point of this certification.
5. The [certification outline](./Certification.md) provides links to resources to help you understand how to develop a RESTful Application.  It also provides a guideline of what you should understand when you complete this certification.


## Business Problem

 A client wants a front end app that find and show YouTube videos. He wants you to design the back end for such an application. He wants the final application to have a favorites list view where each user can see his favorite videos. A user list that lets you see what other people are watching, and for each video to have an internal blurb and comments section to be show when someone accesses a given video. Since this project is designed to test your understanding of JavaScript fundamentals and general Node skills, there will be comments and instructions provided to help with non-Node specific tools. However, the general programming specifics of how to accomplish each step will be left up to you and your coding expertise.

## Initializing

1. Fork and clone the [Node Certification](https://bitbucket.org/stgconsulting/nodecertification/) repository.
    a. Click on the `Plus - create` icon, in the left sidebar.
    b. In the `Get to work` section, click the `Fork this repository` menu item.
    c. Select the `Fork` option near the bottom of the menu.
    d. Click on the `Clone` button at the top of the new repository.
    e. Click on the `Copy` icon next to the URL.
    f. Use a git client of your choice to clone the repository.


2. Create a new branch called `initializing` and checkout this branch. This is where you will begin working on this project.
3. Spin up a new Spring REST app using the [express-generator]https://expressjs.com/en/starter/generator.html) project.

    - Use a package structure with your name somewhere in the structure (e.g. com.stg.sfranson.javabootrestcert).
    - Name the project stg-node-rest-cert

4. Clear out any unneeded files.

5. Jump into the project.
6. Create a file named `node-rest-cert`/Documentation.md` to capture documentation on how to run the application, the provided endpoints and any other information needed.
7. As a first step, create an initial endpoint to retrieve a list (directory) of all users.  The returned data should have at least the basic information about each individual, and does not need to indicate any organization.
8. Document how to access that new user directory endpoint.
9. Save your work and push these changes to Bitbucket and create a Merge Request to merge them to `master`.

## Simple Employee Management

1. Create a new branch called `users` and checkout this branch.
2. Create basic endpoints to perform the basic CRUD (Create, Read, Update, Delete). Users should have at least basic info, a user name and a password. (Extra credit encrypt the password. bcrypt works great)
3. Create endpoints to perform basic CRUD operations on a single user.  ***Note:*** _Be mindful to use generally accepted REST naming conventions for Resource naming._
4. Document the new endpoints and give examples of required payloads.
5. Save your work and push these changes to Bitbucket and create a Merge Request to merge them to `master`.

## Security and Login

Now that we have some users lets make it so that you cant hit anything without a valid token (Not strictly rest but a common node thing to do.)

1. Create a .env file. 
  1. Put your private key if you hashed password in it. 
  2. Also any other info that probably shouldnt be seen. 
  3. Use a lib like [dotenv](https://www.npmjs.com/package/dotenv) to get access to the things in the .env file.
  NOTE: Use "fake" secrets for this project since you will need to commit or send the .env file to the reviewer for the reviewer to run it.
2. Create an auth endpoint that takes a username and password and returns a valid JWT.
3. Put all other endpoints currently in use behind auth middleWhere that validates the JWT.

## My Favorites

1. Create an endpoint that allows one to see there own favorites. A list of titles and links to embed youtube videos.
2. Create CRUT operations for my favorites list.
3. Create endpoints to see other peoples videos. Note you can only edit yours.

## Video Comments

1. Create an endpoint that gives detailed information for a given video. Including title, embedding link, number of people who have it in Favorites and a list of internal comments.
2. Create Crud Operations for a single comment.


## Testing

1. Create a new branch called `testing` and checkout this branch.
1. Create/modify the tests to provide some level of unit/integration testing for your endpoints.

    - Implement them with jest or mocha chi. (Others are acceptable as long as they generate reports.)
    - Provide documentation on how to run the tests with an npm script.
    - Create tests that cover all of the endpoints for the API.

1. Save your work and push these changes to Bitbucket and create a Merge Request to merge them to `master`.


## Finalizing

#**Congratulations**
You have completed the Node Certification!
